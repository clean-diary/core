# Contributing

If you want to contribute, feel free to join at gitlab to the project: https://gitlab.com/clean-diary/core.
Alternatively you can write an email to admin@clean-diary.cloud.