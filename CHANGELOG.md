# Changelog

## Planned features
* possibility to track on which days the run was reset

# 1.6.3
* replace usage of a magic number in the run class with usage of a class variable
* added contribute

## Implemented pre alpha
* Added Changelog, Contribute, Licence file
* initialize maven project
* ignored target and settings folder