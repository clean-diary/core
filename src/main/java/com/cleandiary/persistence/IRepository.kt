package com.cleandiary.persistence

//todo maybe extract into own module
interface IRepository {
    fun <Type> read(id: String, type: Class<Type>): Type
    fun <Type> persist(id: String, type: Class<Type>, value: Type)
}