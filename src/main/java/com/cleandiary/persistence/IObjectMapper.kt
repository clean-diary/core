package com.cleandiary.persistence

//todo maybe extract into own module
interface IObjectMapper {
    fun <Type> read(type: Class<Type>, serialized: String): Type
    fun <Type> write(type: Class<Type>, serializable: Type): String
}