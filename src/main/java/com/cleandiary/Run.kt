package com.cleandiary

import java.util.*

class Run (
        val grade: Grade,
		val days: MutableList<Day> = ArrayList(),
        progress: Int = 0,
        val daysPerRun: Int = 21
){
    var progress: Int
        private set

    init {
        this.progress = progress
    }

    fun reset(){
        this.progress = 0
    }

    fun isComplete():Boolean{
        return this.progress >= this.daysPerRun
    }

    fun add(day: Day){
        if(!isComplete()){
            this.days.add(day)
            this.progress++
        }
    }
}