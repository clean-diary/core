package com.cleandiary

enum class AnswerOptions {
    SUCCESS, FAILURE, UNDECIDED
}