package com.cleandiary.graphics

open class Color(val red: UByte, val green: UByte, val blue: UByte){
    constructor(red: Int, green: Int, blue: Int): this(red.toUByte(), green.toUByte(), blue.toUByte())
}