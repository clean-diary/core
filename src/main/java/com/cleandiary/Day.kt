package com.cleandiary

import java.util.Date

data class Day(val date: Date, val answers: List<AnswerOptions>)