package com.cleandiary

import com.cleandiary.graphics.Color

data class Grade (
	val label: String,
	val color: Color,
	val rules: List<Rule>
)