import com.cleandiary.AnswerOptions
import com.cleandiary.Day
import com.nhaarman.mockito_kotlin.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

internal class DayTest {
    @Test
    fun construction() {
        val answer: List<AnswerOptions> = mock()
        val date: Date = mock()

        val day = Day(date, answer)

        assertThat(date).isEqualTo(day.date)
        assertThat(answer).isEqualTo(day.answers)
    }
}