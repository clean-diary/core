package com.cleandiary.graphics

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.random.nextUBytes

internal class ColorTest {
    var colors = UByteArray(3)

    @BeforeEach
    fun before() {
        this.colors = Random.nextUBytes(3)
    }

    @Test
    fun constructionWithUnsignedBytes() {
        val color = Color(
            colors[0],
            colors[1],
            colors[2]
        )

        testValues(color)
    }

    @Test
    fun constructionWithInteger() {
        val color = Color(
            colors[0].toInt(),
            colors[1].toInt(),
            colors[2].toInt()
        )

        testValues(color)
    }

    fun testValues(color: Color) {
        assertThat(color.red).isEqualTo(colors[0])
        assertThat(color.green).isEqualTo(colors[1])
        assertThat(color.blue).isEqualTo(colors[2])
    }
}