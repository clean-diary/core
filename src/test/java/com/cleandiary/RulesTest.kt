import com.cleandiary.Rule
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class RulesTest {
    @Test
    fun construction() {
        val rule = Rule("TestLabel")

        assertThat(rule.label).isEqualTo("TestLabel")
    }
}
