package com.cleandiary

import com.cleandiary.graphics.Color
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class GradeTest {
    @Test
    fun construction() {
        val label = "Test"
        val color = Color(255u, 0u, 0u)
        val rules = ArrayList<Rule>()

        val grade = Grade(label, color, rules)

        assertThat(label).isEqualTo(grade.label)
        assertThat(color).isEqualTo(grade.color)
        assertThat(rules).isEqualTo(grade.rules)
    }
}