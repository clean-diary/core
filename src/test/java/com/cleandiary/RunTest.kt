import com.cleandiary.Day
import com.cleandiary.Grade
import com.cleandiary.Rule
import com.cleandiary.Run
import com.cleandiary.graphics.Color
import com.nhaarman.mockito_kotlin.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

//todo add reflection tests for progress
internal class RunTest {
    @Test
    fun constructionWithoutDefault() {
        val label = "rot";
        val color: Color = getSomeColorObject()
        val rules: List<Rule> = mock()
        val grade = Grade(label, color, rules)
        val days: MutableList<Day> = mock()

        val run = Run(
            grade,
            days
        )

        assertThat(run.grade).isEqualTo(grade)
        assertThat(run.days).isEqualTo(days)
        assertThat(run.isComplete()).isFalse()
    }

    //to much boilerplate code
    @Test
    fun constructionWithDefault() {
        val label = "rot";
        val color: Color = getSomeColorObject()
        val rules: List<Rule> = mock()
        val grade = Grade(label, color, rules)
        val days: MutableList<Day> = mock()

        val run = Run(
            grade,
            days,
            20
        )

        assertThat(run.grade).isEqualTo(grade)
        assertThat(run.days).isEqualTo(days)
        assertThat(run.isComplete()).isTrue()
    }

    private fun getSomeColorObject(): Color {
        return Color(255u, 0u, 0u)
    }
}