package com.cleandiary

import com.cleandiary.graphics.Color
import com.nhaarman.mockito_kotlin.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class HistoryTest {
    @Test
    fun startWithEmptyList() {
        val history = History()

        assertThat(history.runs.isEmpty()).isTrue()
    }

    @Test
    fun startWithInitialList() {
        val list: List<Run> = mock()

        val history = History(list)

        assertThat(history.runs).isEqualTo(list)
    }

    @Test
    fun addRun() {
        val history = History()
        val run = getSomeRun()

        val addedHistory = history.add(run)

        assertThat(addedHistory.runs).contains(run)
    }


    fun getSomeRun(): Run {
        return Run(getSomeGrade(), ArrayList())
    }

    fun getSomeGrade(): Grade {
        return Grade("Test", Color(255u, 0u, 0u), listOf())
    }
}